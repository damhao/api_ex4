package main

import (
	"API/controller"
	"API/database"
	handler "API/handlers"
	"API/models"
	// "net/http"
	// "github.com/go-chi/chi"
	// "github.com/swaggo/http-swagger"
	// _ "github.com/swaggo/http-swagger/example/go-chi/docs"
)

func main() {
	db := database.InitConnection()
	postModel := models.InitPostModel(db)
	filePath := "/Users/macbook/Documents/API/log/api.log"
	postService := controller.InitPostService(postModel, filePath)
	handler.Handle(postService)
}
