package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "root",
	Short: "rootCmd",
	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
	},
}
var publisherCmd = &cobra.Command{
	Use:   "publisher",
	Short: "redis-pubsub",

	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
		//queue.Pub()
	},
}

var subscriberCmd = &cobra.Command{
	Use:   "subscriber",
	Short: "detail",

	Run: func(cmd *cobra.Command, args []string) {
		// Do Stuff Here
		//queue.Sub()
	},
}

func init() {

	rootCmd.AddCommand(publisherCmd)
	rootCmd.AddCommand(subscriberCmd)

}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
