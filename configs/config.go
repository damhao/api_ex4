package configs

import (
	"fmt"

	"github.com/spf13/viper"
)

func GetConfig() *viper.Viper {
	config := viper.New()
	config.SetConfigName("config")
	config.AddConfigPath("./configs")
	err := config.ReadInConfig()
	if err != nil {
		fmt.Println("Config not found", err)
		return nil
	}
	return config
}
