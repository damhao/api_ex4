package client

import (
	"github.com/go-redis/redis/v8"
)

var Client *redis.Client

func InitRedisClient() {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:8080",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	Client = rdb

	// return rdb
}
