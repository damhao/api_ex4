package utils

import (
	"os"
)

func ExportFile(data, filePath string) error {

	// // Write content to file
	// err := ioutil.WriteFile(filePath, data, 0777)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// Append content to file
	f, err := os.OpenFile(filePath, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err = f.WriteString(data); err != nil {
		return err
	}
	return nil
}
