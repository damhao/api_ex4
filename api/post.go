package api

import (
	"net/http"
)

type PostInterface interface {
	GetPostByID(w http.ResponseWriter, h *http.Request)
	GetAllPosts(w http.ResponseWriter, h *http.Request)
	DeletePostByID(w http.ResponseWriter, h *http.Request)
	CreatePost(w http.ResponseWriter, h *http.Request)
	UpdatePost(w http.ResponseWriter, h *http.Request)
	GetPostByTitle(w http.ResponseWriter, h *http.Request)
	GetPostByTopic(w http.ResponseWriter, h *http.Request)
}
